const tabHeaders = document.querySelectorAll('.tabs-title')
const contentBoxes = document.querySelectorAll('.tabs-content div')

tabHeaders.forEach(function( item, index, array ) {
     item.addEventListener('click', function() {
        // const position = array.indexOf(this)
        console.log(item, index, array);
        contentBoxes.forEach((item) => {
            item.style.display = 'none'
        })

        tabHeaders.forEach((item) => {
            item.classList.remove('active')
        })

        tabHeaders[index].classList.add('active')
        if(index < contentBoxes.length) {
            contentBoxes[index].style.display = 'block'
        }
     })
})

